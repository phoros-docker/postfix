# Postfix in a container 
Small Docker image providing the Postfix MTA server running on Alpine Linux

# Features
* TLS encryption with strong cipher suites (can be weakened when needed)
* Use of multiple domains (when database is properly prepared)
* Easy configuration of additional milter
* Auto-restart on certificate change

This image is part of a multi-container setup with dependencies described below.
Postfix is configured to not support unsecured client connections and with pre-generated 2048 and 4096 bits DH parameters recommended by [Mozilla](https://wiki.mozilla.org/Security/Server_Side_TLS#Pre-defined_DHE_groups).
[The IETF encourages to not use STARTTLS anymore](https://tools.ietf.org/html/rfc8314#section-3) and therefore this image is enforcing TLS for client-to-server connections without the initial plaintext handshake.

# Dependencies / Prerequisites
This image depends on external ressources to fully work. Make sure they are available before you run a container.

* MySQL database
* TLS certificates (only certificate and key needed)
* Dovecot for storing and accessing mails. Running Postfix alone won't do much (okay it would but don't tell anyone!).

# Environment variables
Bold variables indicate that they should be defined or container startup will fail.

| Variable | Type | Purpose |
| -------  | ---- | ------- |
| **TLS_DIRECTORY** | String (unix path) | The directory where TLS certificates are kept without trailing slash. This image tries to find certificates that conform to the Let's Encrypt filename definitions (fullchain.pem). Make sure your certificates are named accordingly or define *TLS_CERT_FILENAME*. |
| TLS_CERT_FILENAME | String (filename) | The filename of your certificate file. Default value is `fullchain.pem`. |
| TLS_CERT_PRIVATE_KEY_FILENAME | String (filename) | The name of your certificates private keyfile. Default value is `privkey.key`. |
| TLS_CA_CERT_FILENAME | String (filename) | The name of the CA of your certificate file. Default value is `ca.pem`. |
| ECC_CERTIFICATE | Boolean [true / false] | Set this flag to `true` if you use ECC certificates. |
| **MAIL_DOMAIN** | String (domain) | The domain where mails are actually received on. Only one domain is accepted, additional domains are forbidden and will cause Postfix to refuse startup. Additional domains are added to your database tables only. |
| **DOVECOT_HOST** | String (FQDN) | The hostname of Dovecot for LMTP, SASL and quota service access. Use the Docker internal hostname format `<container_name>.<internal_network>` like `dovecot.mail`. Docker will resolve this name using its internal DNS resolver. |
| DOVECOT_LMTP_PORT | Integer [1-65535] | The port of Dovecot's LMTP interface. LMTP is used to transfer mails to Dovecot. Default value is `24`. |
| **DOVECOT_SASL_PORT** | Integer [1-65535] | The port of Dovecot's SASL interface. SASL is used to ask Dovecot if credentials are valid. Credentials validation is actually performed by Dovecot as it supports modern password hashing methods. |
| DOVECOT_QUOTA_SERVICE_PORT | Integer [1-65535] | The port of Dovecot's quota service interface. Postfix will contact Dovecot before actually transmitting mails to it to check if there is enough space to store the mail. This can be important as Postfix will reject the mail and tell the remote mail server that transmission was unsuccessful. The sender will be informed that the recipient does not have enough space to receive the mail. Using the quota service is advised. |
| CIPHER_SUITES | String (OpenSSL compatible cipher suites) | Define your own cipher suites here. Use [OpenSSL cipher suite names](https://www.openssl.org/docs/man1.0.2/apps/ciphers.html). Defining this overrides `CIPHER_SUITE_STRENGTH`. |
| CIPHER_SUITE_STRENGTH | String [STRONG] | The cipher suite strength to use. At the moment only `STRONG` is supported. Other values are ignored. When defined Postfix only uses suites that guarantee Perfect Forward Secrecy and the latest TLS protocol version (TLSv1.2 at the moment). |
| TLS_PROTOCOLS_SERVER | String (OpenSSL compatible SSL / TLS protocols) | The SSL / TLS protocols to use for server-to-server communication. Use [OpenSSL protocol names](https://wiki.openssl.org/index.php/SSL_and_TLS_Protocols). Defining this overrides settings pre-defined by `CIPHER_SUITE_STRENGTH`. |
| TLS_PROTOCOLS_CLIENT | String (OpenSSL compatible SSL / TLS protocols) | The SSL / TLS protocols to use for client-to-server communication. Use [OpenSSL protocol names](https://wiki.openssl.org/index.php/SSL_and_TLS_Protocols). Defining this overrides settings pre-defined by `CIPHER_SUITE_STRENGTH`. |
| MAIL_SUBADDRESS_DELIMITER | Single character | The character that splits the local-part and the subaddress of an mail address. This character must be the same as the one Dovecot is using. Default value is ̀̀̀́́́́́́́́́́́́́́`-`. |
| MILTERS | String (space-separated values with format inet:<host>:<port>) | The location of additional milters for Postfix to use. For additional information about milters read the [Postfix documentation](http://www.postfix.org/MILTER_README.html). |

# Database support
This image only supports MySQL-based databases. You need to create several files and mount them into the container to deliver credentials to Postfix (see *Pre-defined directories*). Example files are located [here](/sql/). You need to change all parameters except `query`. It is assumed that you've already imported the `vmail.sql` file into your database from the Dovecot repository.

# Pre-defined directories
This image has a special directory structure where config files and user given files are located. If you mount your own files into this image, make sure you **don't** mount them as read-only as the MySQL related files are chowned to `root:root` to keep them safe. Bold entries are necessary.

- /etc/postfix/ - All config files of Postfix reside here.
  - **/etc/postfix/mysql/** - The MySQL database configuration files for Postfix. You need to mount this folder with your own files in.

# Persistent directories
Use a persistent volume to make sure these files will survive a container removal.

- /var/spool/postfix/ - Postfix uses this folder to temporary save mails that cannot be delivered to Dovecot yet. Make sure you really persist this directory or you will lose mails. And please, please, please use a Docker volume to persist it as Postfix creates a shitload of folders in it with many user and group permissions. Re-creating or even chowning them after each container startup is something I will *NOT* add into this image (yet!). 

# How to use
1. Create a volume to persist mails: `docker volume create postfix-spool`
3. Create the Postfix container `docker create --name postfix --hostname smtp.example.com --publish 25:25 --publish 465:465 --env TLS_DIRECTORY=/etc/ssl/ --env MAIL_DOMAIN=example.com --env DOVECOT_HOST=dovecot.mail --env DOVECOT_SASL_PORT=12345 --env --mount type=bind,source="$(pwd)"/keys/,destination=/etc/ssl/ --mount type=bind,source="$(pwd)"/mysql/,destination=/etc/postfix/mysql/ --mount type=volume,source=postfix-spool,destination=/var/spool/postfix/ registry.gitlab.com/phoros-docker/postfix:latest`
4. Attach the container to the database Docker network (recommended): `docker network connect mysql postfix`
5. Attach the container to the mail Docker network: `docker network connect mail postfix`
6. Start the container: `docker start postfix`

This example uses port 12345 as the SASL port for communication between Dovecot and Postfix. You can choose any port you like. TLS certificate, CA file and private key file is mounted from the folder `keys/` which is located in the current directory. SQL related config files for Postfix is also located in the current directory in its own folder `mysql/`
