#
# - BASIC CONFIGURATION -
#

smtputf8_enable = no
smtpd_banner = $myhostname ESMTP $mail_name
biff = no
append_dot_mydomain = no
disable_vrfy_command = yes
readme_directory = no
mailbox_size_limit = 0
message_size_limit = 52428800
recipient_delimiter = {{ default "-" .Env.RECIPIENT_DELIMITER }}
maillog_file = /dev/stdout

#
# - NETWORK CONFIGURATION -
#

mynetworks = 127.0.0.0/8 [::1]/128
inet_interfaces = all
inet_protocols = all
myhostname = {{ .Env.HOSTNAME }}
mydomain = {{ .Env.MAIL_DOMAIN }}
mydestination = $myhostname localhost localhost.$mydomain

#
# - MAIL QUEUE CONFIGURATION -
#

maximal_queue_lifetime = 2d
bounce_queue_lifetime = 12h
maximal_backoff_time = 30m
minimal_backoff_time = 15m
queue_run_delay = 10m

#
# - TLS CONFIGURATION -
#

# -- Global --
tls_ssl_options = NO_COMPRESSION
tls_high_cipherlist = {{ if .Env.CIPHER_SUITES }}{{ .Env.CIPHER_SUITES }}{{ else }}{{ if (.Env.CIPHER_SUITE_STRENGTH) and eq .Env.CIPHER_SUITE_STRENGTH "STRONG" }}ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-CCM8:ECDHE-ECDSA-AES256-CCM:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA256:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256{{ else }}ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS{{ end }}{{ end }}
tls_preempt_cipherlist = yes
tls_eecdh_strong_curve = prime256v1
tls_eecdh_ultra_curve = secp384r1

# -- Outgoing connections --
smtp_tls_security_level = dane
{{ if (exists "/etc/postfix/mysql/tls_policy.cf") }}smtp_tls_policy_maps = mysql:/etc/postfix/mysql/tls_policy.cf{{ end }}
smtp_dns_support_level = dnssec
smtp_tls_note_starttls_offer = yes
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache 
smtp_tls_mandatory_protocols = {{ if .Env.TLS_PROTOCOLS_SERVER }}{{ .Env.TLS_PROTOCOLS_SERVER }}{{ else }}{{ if (.Env.CIPHER_SUITE_STRENGTH) and eq .Env.CIPHER_SUITE_STRENGTH "STRONG" }}!SSLv2, !SSLv3, !TLSv1, !TLSv1.1{{ else }}!SSLv2, !SSLv3{{ end }}{{ end }}

# -- Incoming client connections --
smtpd_use_tls = yes
#smtpd_tls_loglevel = 3
smtpd_tls_security_level = may
smtpd_tls_mandatory_ciphers = high
smtpd_tls_eecdh_grade = strong
smtpd_tls_mandatory_protocols = {{ if .Env.TLS_PROTOCOLS_CLIENT }}{{ .Env.TLS_PROTOCOLS_CLIENT }}{{ else }}{{ if (.Env.CIPHER_SUITE_STRENGTH) and eq .Env.CIPHER_SUITE_STRENGTH "STRONG" }}!SSLv2, !SSLv3, !TLSv1, !TLSv1.1{{ else }}!SSLv2, !SSLv3{{ end }}{{ end }}
smtpd_tls_cert_file = /etc/postfix/certs/cert.pem
smtpd_tls_key_file = /etc/postfix/certs/cert.key
smtpd_tls_CAfile = /etc/postfix/certs/ca.crt
smtpd_tls_dh1024_param_file = ${config_directory}/crypto/dh{{ if (.Env.CIPHER_SUITE_STRENGTH) and eq .Env.CIPHER_SUITE_STRENGTH "STRONG" }}4096{{ else }}2048{{ end }}.pem
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtpd_tls_auth_only = yes
smtpd_tls_received_header = yes

# -- Rate limiting --
smtpd_client_connection_rate_limit = 5
smtpd_client_new_tls_session_rate_limit = 5
smtpd_client_auth_rate_limit = 3

#
# - POSTFIX SASL CONFIGURATION -
#
# For client login
#
smtpd_sasl_type = dovecot
smtpd_sasl_path = inet:{{ .Env.DOVECOT_HOST }}:{{ .Env.DOVECOT_SASL_PORT }}
smtpd_sasl_auth_enable = yes
smtpd_sasl_local_domain = $myhostname
smtpd_sasl_security_options = noanonymous
smtpd_sasl_authenticated_header = yes

#
# - POSTFIX RESTRICTIONS -
#

# -- Privacy features --
unverified_recipient_reject_reason = Address lookup failed

# -- Incoming messages --
smtpd_helo_required = yes
smtpd_helo_restrictions =   permit_mynetworks,
                            reject_invalid_hostname

smtpd_sender_restrictions = permit_mynetworks,
                            permit_sasl_authenticated,
                            reject_unknown_sender_domain,
                            reject_non_fqdn_sender,
                            reject_unlisted_sender,
                            reject_unauth_pipelining,
                            permit

smtpd_recipient_restrictions =  permit_mynetworks,
                                permit_sasl_authenticated,
{{ if (exists "/etc/postfix/mysql/recipient_access.cf") }}                                check_recipient_access mysql:/etc/postfix/mysql/recipient_access.cf,{{ end }}
                                reject_unauth_destination,
                                reject_unauth_pipelining,
                                reject_unknown_reverse_client_hostname,
                                reject_invalid_helo_hostname,
                                reject_non_fqdn_helo_hostname,
                                reject_non_fqdn_sender,
                                reject_non_fqdn_recipient,
                                reject_unknown_sender_domain,
                                reject_unknown_recipient_domain,
                                reject_invalid_hostname,
{{ if (.Env.DOVECOT_QUOTA_SERVICE_PORT) }}                                check_policy_service inet:{{ .Env.DOVECOT_HOST }}:{{ .Env.DOVECOT_QUOTA_SERVICE_PORT }},{{ end }}
                                permit

smtpd_data_restrictions = reject_unauth_pipelining

# -- Outgoing messages --
smtpd_client_restrictions = permit_mynetworks,
                            permit_sasl_authenticated,
                            reject_rbl_client zen.spamhaus.org,
                            permit

# -- Postscreen --
postscreen_access_list = permit_mynetworks
postscreen_blacklist_action = drop
postscreen_greet_action = drop
postscreen_dnsbl_threshold = 2
postscreen_dnsbl_sites =    bl.spamcop.net*1,
                            ix.dnsbl.manitu.net*2,
                            zen.spamhaus.org*2,
                            b.barracudacentral.org*1
postscreen_dnsbl_action = drop

#
# - POSTFIX MAIL MANIPULATION -
#

# -- Outgoing messages --
smtp_header_checks = regexp:/etc/postfix/smtp_header_checks

# -- Milter configuration --
{{ if .Env.MILTERS }}milter_default_action = accept
milter_protocol = 6
smtpd_milters = {{ .Env.MILTERS }}
non_smtpd_milters = $smtpd_milters
milter_mail_macros = i {mail_addr} {client_addr} {client_name} {auth_authen} {auth_type}{{ end }}

#
# - POSTFIX VIRTUAL TRANSPORT -
#

# -- Transport method --
virtual_transport = lmtp:inet:{{ .Env.DOVECOT_HOST }}:{{ .Env.DOVECOT_LMTP_PORT }}

# -- Lookup files --
virtual_mailbox_domains = mysql:/etc/postfix/mysql/domains.cf
virtual_mailbox_maps = mysql:/etc/postfix/mysql/mailboxes.cf
virtual_alias_maps = mysql:/etc/postfix/mysql/aliases.cf
local_recipient_maps = $virtual_mailbox_maps
