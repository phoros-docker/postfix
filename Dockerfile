FROM alpine:latest

ENV S6_OVERLAY_VERSION v1.22.1.0
ENV DOCKERIZE_VERSION v2.1.0

# Terminate all s6 childs when init stage fails
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

# Update package list and install needed packages, download s6-overlay, download dockerize
RUN apk add --update --no-cache --no-progress bash ca-certificates inotify-tools postfix postfix-mysql \
    && wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz \
    && tar -C / -xzvf s6-overlay-amd64.tar.gz \
    && rm s6-overlay-amd64.tar.gz \
    && wget https://github.com/presslabs/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# Copy required assets for s6-overlay
COPY ./assets/s6-overlay/ /etc/

# Copy configuration
COPY ./config/ /

# Fix permissions
RUN chmod 755 /etc/ \
    && chmod -R a=r,u+w,a+X /etc/postfix/

EXPOSE 25 465
VOLUME ["/var/spool/postfix/"]
ENTRYPOINT ["/init"]
